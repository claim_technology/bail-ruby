module Bail
  class Authentication
    AUTH_URL = 'https://v1-api.bail.claims/api/oauth/token'

    def initialize(client_id, client_secret)
      @client_id = client_id
      @client_secret = client_secret
    end

    def token
      return nil unless @client_secret && @client_id

      responce = RestClient::Request.execute(request_options)
      JSON.parse(responce.body)['access_token']
    end

    private

    def request_options
      {
        url: AUTH_URL,
        payload: payload,
        method: 'post',
        headers: {},
        open_timeout: Bail.open_timeout,
        timeout: Bail.read_timeout
      }
    end

    def payload
      {
        "grant_type": "client_credentials",
        "client_id": @client_id,
        "client_secret": @client_secret,
        "scope": "*"
      }
    end
  end
end
