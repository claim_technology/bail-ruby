module Bail
  class Api
    def initialize(options = {})
      @api_key = options[:api_key]
    end

    def incident
      Bail::Incident.new(@api_key)
    end
  end
end