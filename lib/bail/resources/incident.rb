module Bail
  class Incident < Resource
    def create(payload)
      post(url: url_for('incidents'), payload: payload)
    end

    def show(incident_id)
      get(url: url_for("incidents/#{incident_id}"))
    end

    def types
      get(url: url_for("incident-types"))
    end

    def party(incident_id, payload)
      post(url: url_for("incidents/#{incident_id}/party"), payload: payload)
    end

    def resolution(incident_id, party_id)
      get(url: url_for("incidents/#{incident_id}/#{party_id}"))
    end

    def query
      Bail::Query.new(@api_key)
    end
  end
end
