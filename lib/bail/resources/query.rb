module Bail
  class Query < Resource
    def start(incident_id, party_id, payload)
      simple_post(url: url_for("incidents/#{incident_id}/#{party_id}/details/start"), payload: payload)

    end

    def answer(incident_id, party_id, payload)
      simple_post(url: url_for("incidents/#{incident_id}/#{party_id}/details/answer"), payload: payload)
    end
  end
end