module Bail
  module Configuration
    REGION_HOSTS = {
      us: 'v1-api.bail.claims/api'
    }.freeze

    attr_accessor :client_id, :client_secret, :region, :open_timeout, :read_timeout, :api_version

    def self.extended(base)
      base.reset
    end

    def configure
      yield self
    end

    def api_key
      Authentication.new(client_id, client_secret).token
    end

    def reset
      self.client_id = nil
      self.client_secret = nil
      self.region = nil
      self.open_timeout = 30
      self.read_timeout = 80
      self.api_version = '1.0'
      RestClient.log = nil
    end

    def logger=(log)
      unless log.respond_to?(:<<)
        raise "#{log.class} doesn't seem to behave like a logger!"
      end

      RestClient.log = log
    end

    def logger
      RestClient.log ||= NullLogger.new
    end

    def endpoint
      region_host = region ? REGION_HOSTS[region.downcase.to_sym] : 'v1-api.bail.claims/api'
      unless region_host
        raise "The region \"#{region.downcase}\" is not currently supported"
      end

      "https://#{region_host}/#{api_version}/"
    end
  end
end