require 'json'
require 'rack'
require 'rest-client'
require 'openssl'

require 'bail/version'
require 'bail/configuration'
require 'bail/authentication'
require 'bail/errors/bail_error'
require 'bail/errors/request_error'
require 'bail/errors/server_error'
require 'bail/errors/connection_error'
require 'bail/null_logger'
require 'bail/api'
require 'bail/resource'
require 'bail/resources/incident'
require 'bail/resources/query'

module Bail
  extend Configuration
end