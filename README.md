# Bail

A thin wrapper for Bail's API.

This gem supports `1.0` of the Bail API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bail', '~> 0.1.0'
```

The gem is compatible with Ruby 2.2.0 and onwards. Earlier versions of Ruby have [reached end-of-life](https://www.ruby-lang.org/en/news/2017/04/01/support-of-ruby-2-1-has-ended/), are no longer supported and no longer receive security fixes.

## Configuration

There are 7 configuration options:

```ruby
Bail.configure do |config|
  config.client_id = 'client_id'
  config.client_secret = 'client_secret'
  config.api_version = '1.0'
  config.logger = Logger.new(STDOUT)
  config.open_timeout = 30
  config.read_timeout = 80
  config.region = nil
end
```

### Regions

The gem will use the default region if no region is specified.

To specify the US region do:
`config.region = :us`


## Usage

You can make API calls by using an instance of the `API` class:

```ruby
api = Bail::Api.new
```

Alternatively, you can set an API key here instead of in the initializer:

```ruby
api = Bail::Api.new(api_key: 'API_KEY')
```

#### Incident

Incidents are the object upon which Bail checks are performed.

```ruby
api.incident.create(params)                         # => Creates an incident
api.incident.show('incident_id')                    # => Shows an incident
api.incident.types                                  # => Shows an incident types
api.incident.party('incident_id', params)           # => Creates a party
api.incident.resolution('incident_id', 'party_id')  # => 
api.incident.query                                  # => Returns an Query object
```

#### Query

Documents provide supporting evidence for Bail checks.

```ruby
query = api.incident.query

query.start('incident_id', 'party_id', params)  # => Creates a query
query.answer('incident_id', 'party_id', params) # => Creates an answer
```

**Note:** To start a query you must have created two parties against an incident
          with types claimant and third party.

### Error Handling

There are three classes of errors raised by the library, all of which subclass `Bail::BailError`:
- `Bail::ServerError` is raised whenever Bail returns a `5xx` response
- `Bail::RequestError` is raised whenever Bail returns any other kind of error
- `Bail::ConnectionError` is raised whenever a network error occurs (e.g., a timeout)

All three error classes provide the `response_code`, `response_body`, `json_body`, `type` and `fields` of the error (although for `Bail::ServerError` and `Bail::ConnectionError` the last three are likely to be `nil`).

```ruby
def create_applicant
  api.applicant.create(params)
rescue Bail::RequestError => e
  e.type          # => 'validation_error'
  e.fields        # => { "email": { "messages": ["invalid format"] } }
  e.response_code # => '422'
end
```
