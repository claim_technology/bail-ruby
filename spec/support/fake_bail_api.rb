require 'sinatra/base'

class FakeBailAPI < Sinatra::Base
  post '/oauth/token' do
    json_response(201, 'authentication.json')
  end

  # Bail::Incident.new.show('ec223a2f-e450-44dd-8aff-f06021e14a89')
  get '/1.0/incidents/:id' do
    json_response(200, 'incident.json')
  end

  # Bail::Incident.new.create("client_reference": "ExampleCase12300", "date": "07/11/2018", "time": "14:15")
  post '/1.0/incidents' do
    json_response(201, 'incidents.json')
  end

  get '/1.0/incident-types' do
    json_response(200, 'incident_types.json')
  end

  # Bail::Incident.new.party('ec223a2f-e450-44dd-8aff-f06021e14a89', {"name": "Test Party", "vehicle_registration": "Not Provided", "type": { "name": "witness" }})
  post '/1.0/incidents/:id/party' do
    json_response(201, 'party.json')
  end

  get '/1.0/incidents/:id/:party_id' do
    json_response(200, 'resolution.json')
  end

  post '/1.0/incidents/:id/:party_id/details/start' do
    json_response(201, 'start.json')
  end

  post '/1.0/incidents/:id/:party_id/details/answer' do
    json_response(201, 'answer.json')
  end

  get '/1.0/4xx_response' do
    json_response(422, '4xx_response.json')
  end

  get '/1.0/unparseable_response' do
    content_type :json
    status 504
    ''
  end

  private

  def json_response(response_code, file_name)
    content_type "application/json; charset=utf-8"
    status response_code
    File.open(File.dirname(__FILE__) + '/fixtures/' + file_name, 'rb').read
  end
end
