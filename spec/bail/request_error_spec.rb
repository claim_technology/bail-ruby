require 'spec_helper'

describe Bail::RequestError do
  subject(:error) do
    described_class.new(
      failed_response['error']['message'],
      response_code: 401,
      response_body: failed_response.to_json
    )
  end

  let(:failed_response) { { 'error' => ''} }

  its(:type) { is_expected.to eq(nil) }
  its(:response_code) { is_expected.to eq(401) }
  its(:response_body) { is_expected.to eq(failed_response.to_json) }
  its(:json_body) { is_expected.to eq(failed_response) }
  its(:fields) { is_expected.to eq(nil) }
end
