require 'spec_helper'

describe Bail::Api do
  subject(:api) { described_class.new }

  describe 'given a single-word resource' do
    specify { expect(api.incident).to be_a(Bail::Incident) }
  end

  describe 'given a multi-word resource' do
    specify { expect(api.incident.query).to be_a(Bail::Query) }
  end

  describe 'given an unknown resource' do
    specify { expect { api.blood_test }.to raise_error(NameError) }
  end

  describe 'given no API key' do
    it 'uses nil for the resource API key' do
      expect(Bail::Incident).to receive(:new).with(nil)
      api.incident
    end
  end

  describe 'given an API key' do
    let(:api_key) { 'some_key' }

    subject(:api) { described_class.new(api_key: api_key) }

    it 'uses that key to create the resource' do
      expect(Bail::Incident).to receive(:new).with(api_key)
      api.incident
    end
  end
end
