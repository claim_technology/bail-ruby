require 'spec_helper'

describe Bail do
  subject(:bail) { described_class }
  after(:each) { bail.reset }

  context 'configuration' do
    describe "default values" do
      describe ".api_key" do
        subject { bail.api_key }
        it { is_expected.to be_nil }
      end

      describe ".endpoint" do
        subject { bail.endpoint }
        it { is_expected.to eq('https://api.bail.automatedinsurancesolutions.co.uk/api/1.0/') }
      end

      describe ".logger" do
        subject { bail.logger }
        it { is_expected.to be_an_instance_of(Bail::NullLogger) }
      end
    end

    describe "setting an client id" do
      it 'changes the configuration to the new value' do
        bail.client_id = 'some_key'
        expect(bail.client_id).to eq('some_key')
      end
    end

    describe "setting an client secret" do
      it 'changes the configuration to the new value' do
        bail.client_secret = 'some_secret'
        expect(bail.client_secret).to eq('some_secret')
      end
    end

    describe "setting the API version" do
      it 'changes the configuration to the new value' do
        bail.api_version = '2.0'
        expect(bail.api_version).to eq('2.0')
        expect(bail.endpoint).to eq('https://api.bail.automatedinsurancesolutions.co.uk/api/2.0/')
      end
    end

    describe 'using the US region' do
      it 'should change endpoint' do
        bail.region = 'us'
        expect(bail.endpoint).to eq('https://api.bail.automatedinsurancesolutions.co.uk/api/1.0/')
      end
    end

    describe '.logger' do
      context 'when an option is passed' do
        context 'when the option passed behaves like a logger' do
          let(:logger_like) { double('LoggerLike', :<< => nil) }

          it 'returns the option' do
            bail.logger = logger_like
            expect(bail.logger).to eq(logger_like)
          end
        end

        context 'when the option passed does not behave like a logger' do
          let(:non_logger) { double('NotLogger') }

          it 'raises an error' do
            expect { bail.logger = non_logger }.
              to raise_error(
                "#{non_logger.class} doesn't seem to behave like a logger!"
              )
          end
        end
      end
    end
  end
end
