# coding: utf-8

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bail/version'

Gem::Specification.new do |spec|
  spec.name          = 'bail'
  spec.version       = Bail::VERSION
  spec.authors       = ['Eugene Kuznietsov']
  spec.email         = ['eugene.kuznietsov@figitalsuits.co']
  spec.summary       = 'A wrapper for Bail API'
  spec.description   = "A thin wrapper for Bail API."
  spec.homepage      = 'https://bitbucket.org/claim_technology/bail/src'
  spec.license       = 'MIT'

  spec.files         = ['lib/bail.rb']
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']
  spec.required_ruby_version = ">= 2.2.0"

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rspec', '~> 3.1'
  spec.add_development_dependency 'rspec-its', '~> 1.2'
  spec.add_development_dependency 'rubocop', '~> 0.57.0'
  spec.add_development_dependency 'sinatra', '~> 1.4'
  spec.add_development_dependency 'webmock', '~> 3.0'

  spec.add_dependency 'rack', '>= 1.6.0'
  spec.add_dependency 'rest-client', '~> 2.0'
end
